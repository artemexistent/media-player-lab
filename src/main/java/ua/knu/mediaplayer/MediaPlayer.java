package ua.knu.mediaplayer;

import static com.google.common.base.Preconditions.checkArgument;
import static com.teamdev.jxbrowser.engine.RenderingMode.HARDWARE_ACCELERATED;
import static javax.swing.SwingUtilities.invokeLater;

import com.teamdev.jxbrowser.browser.Browser;
import com.teamdev.jxbrowser.browser.callback.InjectJsCallback;
import com.teamdev.jxbrowser.browser.callback.InjectJsCallback.Response;
import com.teamdev.jxbrowser.engine.Engine;
import com.teamdev.jxbrowser.engine.EngineOptions;
import com.teamdev.jxbrowser.engine.ProprietaryFeature;
import com.teamdev.jxbrowser.frame.Frame;
import com.teamdev.jxbrowser.js.JsObject;
import com.teamdev.jxbrowser.view.swing.BrowserView;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.WindowConstants;

public final class MediaPlayer extends JPanel {

    private static final String BTN_MUTE_TEXT = "Mute";
    private static final String BTN_UNMUTE_TEXT = "Unmute";

    private static final String BTN_PLAY_TEXT = "Play";
    private static final String BTN_PAUSE_TEXT = "Pause";

    public static void main(String[] args) {
        invokeLater(() -> {
            MediaPlayer mediaPlayer = new MediaPlayer();

            JFrame frame = new JFrame("Java Media Player");
            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    mediaPlayer.dispose();
                }
            });
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            frame.add(mediaPlayer, BorderLayout.CENTER);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        });
    }

    private final Engine engine;
    private JsPlayer player;

    private MediaPlayer() {
        engine = Engine.newInstance(
                EngineOptions.newBuilder(HARDWARE_ACCELERATED)
                        .licenseKey(
                                "1BNDHFSC1G4PSE05B0M609QBRW14FPVU64W83MJNAIDT1G8WEJHKG3T3DS1DY2643YFBX8")
                        .enableProprietaryFeature(ProprietaryFeature.H_264)
                        .enableProprietaryFeature(ProprietaryFeature.AAC)
                        .enableAutoplay()
                        .build());
        Browser browser = engine.newBrowser();

        browser.set(InjectJsCallback.class, params -> {
            Frame frame = params.frame();
            JsObject window = frame.executeJavaScript("window");
            if (window != null) {
                player = new JsPlayer(frame);
                window.putProperty("java", player);
            }
            return Response.proceed();
        });

        URL resource = MediaPlayer.class.getResource("/media.html");
        if (resource != null) {
            browser.navigation().loadUrlAndWait(resource.toString());
        }

        BrowserView view = BrowserView.newInstance(browser);
        view.setPreferredSize(new Dimension(1280, 720));

        setLayout(new BorderLayout());
        add(view, BorderLayout.CENTER);
        add(playbackControls(), BorderLayout.SOUTH);
    }

    private void dispose() {
        engine.close();
    }


    private static String formatElapsedTime(int elapsedSeconds) {
        checkArgument(elapsedSeconds >= 0);
        int hours = elapsedSeconds / 3600;
        int minutes = (elapsedSeconds % 3600) / 60;
        int seconds = elapsedSeconds % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private JComponent playbackControls() {
        JPanel controls = new JPanel();
        controls.add(createPlaybackTimeLabels());
        controls.add(createMuteUnmuteButton());
        controls.add(createVolumeSlider());

        JPanel result = new JPanel();
        result.setLayout(new BorderLayout());
        result.add(createPlayPauseButton(), BorderLayout.WEST);
        result.add(createPlaybackSlider(), BorderLayout.CENTER);
        result.add(controls, BorderLayout.EAST);
        return result;
    }

    private JSlider createVolumeSlider() {
        JSlider volume = new JSlider(0, 100, ((int) (player.volume() * 100)));
        volume.addChangeListener(e -> player.volume((double) volume.getValue() / 100));
        return volume;
    }

    private JComponent createPlaybackTimeLabels() {
        JLabel currentTimeLabel = new JLabel(formatElapsedTime(player.currentTime().intValue()));
        JLabel durationLabel = new JLabel(formatElapsedTime(player.duration().intValue()));

        player.onTimeUpdated(currentTime -> invokeLater(
                () -> currentTimeLabel.setText(formatElapsedTime(currentTime.intValue()))));

        JPanel result = new JPanel();
        result.add(currentTimeLabel);
        result.add(new JLabel("/"));
        result.add(durationLabel);
        return result;
    }

    private JComponent createPlaybackSlider() {
        JSlider playbackSlider = new JSlider(0, player.duration().intValue(), 0);
        playbackSlider.addChangeListener(e -> {
            if (playbackSlider.getValueIsAdjusting()) {
                player.currentTime(playbackSlider.getValue());
            }
        });
        player.onTimeUpdated(
                currentTime -> invokeLater(() -> playbackSlider.setValue(currentTime.intValue())));

        return playbackSlider;
    }

    private JButton createMuteUnmuteButton() {
        JButton muteButton = new JButton(BTN_MUTE_TEXT);
        muteButton.addActionListener(e -> {
            if (player.isMuted()) {
                player.unmute();
            } else {
                player.mute();
            }
        });
        player.onVolumeChanged(
                muted -> muteButton.setText(muted ? BTN_UNMUTE_TEXT : BTN_MUTE_TEXT));
        return muteButton;
    }

    private JComponent createPlayPauseButton() {
        JButton playButton = new JButton(BTN_PLAY_TEXT);
        playButton.addActionListener(e -> {
            if (player.isPlaying()) {
                player.pause();
            } else {
                player.play();
            }
        });

        player.onPlaybackStarted(unused -> playButton.setText(BTN_PAUSE_TEXT));
        player.onPlaybackPaused(unused -> playButton.setText(BTN_PLAY_TEXT));

        JPanel panel = new JPanel();
        panel.add(playButton);
        return panel;
    }
}
